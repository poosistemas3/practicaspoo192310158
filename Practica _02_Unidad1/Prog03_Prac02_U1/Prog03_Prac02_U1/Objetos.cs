﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog03_Prac02_U1
{
    class Objetos
    {
        double AreaRueda, PerimetroRueda, RadioRueda;
        public void Rueda()
        {
            RadioRueda = 10;
            PerimetroRueda = (3.1416 * 2) * RadioRueda;
            Console.WriteLine("El perimetro de la rueda es: " + PerimetroRueda);
            AreaRueda = 3.1416 * RadioRueda * RadioRueda;
            Console.WriteLine("El area de la rueda es: " + AreaRueda);
        }
        double AreaMoneda, PerimetroMoneda, RadioMoneda;
        public void Moneda()
        {
            RadioMoneda = 1;
            PerimetroMoneda = (3.1416 * 2) * RadioMoneda;
            Console.WriteLine("El perimetro de la moneda es: " + PerimetroMoneda);
            AreaMoneda = 3.1416 * RadioMoneda * RadioMoneda;
            Console.WriteLine("El area de la moneda es: " + AreaMoneda);
              
        }
    }
}
