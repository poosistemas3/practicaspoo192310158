﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog02_Prac02_U1
{
    class Circunferencia
    {
        private double Perimetro;
        private double Area;
        private double Radio;

        public void Calcular_area()
        {
            Console.WriteLine("Ingresa el radio para el area");
            Radio = double.Parse(Console.ReadLine());
         
            Area = 3.1416 * Radio * Radio;
            Console.WriteLine("El area es igual a: "+Area);
        }

        public void Calcular_perimetro()
        {
            Console.WriteLine("Ingresa el radio para el perimetro");
            Radio = double.Parse(Console.ReadLine());
            
            Perimetro = (3.1416 * 2) * Radio;
            Console.WriteLine("El perimetro es: "+Perimetro);
        }
    }
}
